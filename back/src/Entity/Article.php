<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
class Article
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['public_view'])]
    private int $id;

    #[Assert\Length(min: 3, max: 255)]
    #[ORM\Column(length: 255)]
    #[Groups(['public_view'])]
    private string $name;

    #[Assert\NotBlank]
    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['public_view'])]
    private string $content;

    #[ORM\Column]
    #[Groups(['author_view'])]
    private bool $isPublished;

    #[ORM\Column]
    #[Groups(['public_view'])]
    private \DateTimeImmutable $createdAt;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['public_view'])]
    private User $author;

    public function __construct(string $name, string $content, User $author, bool $isPublished = true)
    {
        $this->name = $name;
        $this->content = $content;
        $this->isPublished = $isPublished;
        $this->author = $author;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function update(string $name, string $content, bool $isPublished): void
    {
        $this->name = $name;
        $this->content = $content;
        $this->isPublished = $isPublished;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }
}
