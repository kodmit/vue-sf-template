<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    public function __construct(
        private ArticleRepository $articleRepository,
        private EntityManagerInterface $entityManager
    ) {}

    #[Route('/articles', methods: 'GET')]
    public function getArticles(): JsonResponse
    {
        return $this->json(
            $this->articleRepository->findBy(['isPublished' => true]),
            Response::HTTP_OK,
            [],
            ['groups' => 'public_view']
        );
    }

    #[Route('/articles', methods: 'POST')]
    public function createArticle(Request $request): JsonResponse
    {
        $payload = $this->getPayload($request);

        $article = new Article(
            $payload->get('title'),
            $payload->get('content'),
            $this->getUser()
        );

        $this->entityManager->persist($article);
        $this->entityManager->flush();

        return $this->json($article, Response::HTTP_CREATED);
    }

    #[Route('/articles/{id}', methods: 'DELETE')]
    public function deleteArticle(int $id): JsonResponse
    {
        $article = $this->articleRepository->find($id);

        if (null === $article) {
            return $this->json(['error' => 'Article not found'], Response::HTTP_NOT_FOUND);
        }

        $this->entityManager->remove($article);
        $this->entityManager->flush();

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/articles/{id}', methods: 'PUT')]
    public function updateArticle(int $id, Request $request): JsonResponse
    {
        $article = $this->articleRepository->find($id);

        if (null === $article) {
            return $this->json(['error' => 'Article not found'], Response::HTTP_NOT_FOUND);
        }

        if ($article->getAuthor() !== $this->getUser()) {
            return $this->json(['error' => 'You are not allowed to update this article'], Response::HTTP_FORBIDDEN);
        }

        $payload = $this->getPayload($request);

        $article->update(
            $payload->get('name'),
            $payload->get('content'),
            $payload->get('isPublished')
        );

        $this->entityManager->flush();

        return $this->json(
            $article,
            Response::HTTP_OK,
            [],
            ['groups' => 'public_view']
        );
    }
}
